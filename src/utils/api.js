export default {
  host: "https://jsonplaceholder.typicode.com",
  request(url) { return fetch(url).then(response => response.json()) }
};