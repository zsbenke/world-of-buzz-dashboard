import api from "../utils/api.js";
import Item from "./Item.js";

export default class User {
  constructor(id, fullName, username) {
    this.id = id;
    this.fullName = fullName;
    this.username = username;
  }

  getPhoto() {
    let url = userRouter.photos(this.id);

    return new Promise((resolve, reject) => {
      api.request(url).then(data => {
        let photo = data[0];

        this.photo = {};
        this.photo.thumbnailUrl = photo.thumbnailUrl;
        this.photo.url = photo.url;

        resolve(this.photo)
      });
    });
  }

  get firstName() {
    return this.fullName.split(" ")[0];
  }

  get lastName() {
    return this.fullName.split(" ")[1];
  }

  getItems() {
    let url = userRouter.items(this.id);

    return new Promise((resolve, reject) => {
      api.request(url).then(data => {
        this.items = data.map(itemData => { return new Item(itemData.id, itemData.title) })
        resolve(this.items)
      });
    });
  }

  static find(userID) {
    let url = userRouter.show(userID);

    return new Promise((resolve, reject) => {
      api.request(url).then(data => {
        let user = new User(data.id, data.name, data.username);

        resolve(user);
      })
    });
  }

  static all() {
    let url = userRouter.index

    return new Promise((resolve, reject) => {
      api.request(url).then(data => {
        let users = data.map(userData => { return new User(userData.id, userData.name, userData.username) })
        resolve(users)
      });
    })
  }
};

let userRouter = {
  index: api.host + '/users',
  photos(userID) { return `${api.host}/users/${userID}/photos`; },
  show(userID) { return `${api.host}/users/${userID}`; },
  items(userID) { return `${api.host}/users/${userID}/albums` }
}