import Moment from "moment";

export default {
  accounts: [
    { title: "XYZ eBay account", available: true },
    { title: "Luke Skywalker eBay account", available: true },
    { title: "Han Solo 1978 eBay account", available: true },
    { title: "Anakin Skywalker eBay account", available: false }
  ],
  notifications: [
    { title: "eBay order job finished successfully", timestamp: "7:25"},
    { title: "eBay order job finished successfully", timestamp: "8:25"},
    { title: "eBay order job finished successfully", timestamp: "9:25"},
    { title: "eBay order job finished successfully", timestamp: "10:25"}
  ],
  inventoryLevels() {
    let levels = [];
    let i = 0;

    while (i < 10) {
      let date = Moment().subtract(i, 'days').hours(0).minutes(0).format("MM.DD.");
      let level = parseInt(Math.random() * 100);

      levels.push({ date: date, level: level });
      i++;
    }

    return levels;
  }
};
