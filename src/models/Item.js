import api from "../utils/api.js";

export default class Item {
  constructor(id, title) {
    this.id = id;
    this.title = title;
    this.status = Item.statuses()[Math.floor(Math.random() * Item.statuses().length)];
  }

  static all() {
    let url = itemRouter.index

    return new Promise((resolve, reject) => {
      api.request(url).then(data => {
        let items = data.map(itemData => { return new Item(itemData.id, itemData.title) });
        resolve(items)
      });
    })
  }

  static statuses() {
    return Item.statusColors().map(statusColor => statusColor.status);
  }

  static statusColors() {
    return [
      { status: "active", color: "#A7CDA7" },
      { status: "sold", color: "#199DD9" },
      { status: "unsold", color: "#E9E9E9" },
      { status: "scheduled", color: "#344159" }
    ]
  }
}

let itemRouter = {
  index: api.host + '/albums'
}