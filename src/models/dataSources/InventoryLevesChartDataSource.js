export default class InventoryLevelsChartDataSource {
  constructor(items) {
    let levels = items.map(item => item.level);
    let data = {};
    let dataset = {
      label: "Items listed on eBay",
      data: levels,
      backgroundColor: "#A7CDA7"
    };

    data.labels = items.map(item => item.date);
    data.datasets = [];
    data.datasets.push(dataset)

    return data;
  }
}