import Item from "../Item.js";

export default class ListingStatisticsDataSource {
  constructor(items) {
    let statuses = Item.statuses();

    this.listingStats = []
    this.totalCount = items.length

    for (let status of statuses) {
      let sum = items.filter(item => item.status == status).length;
      let listingStat = new ListingStat(status, sum, (sum / this.totalCount * 100).toFixed(0));
      this.listingStats.push(listingStat);
    }
  }

  get chartData() {
    let sumsOflistingStats = this.listingStats.map(item => item.sum);

    let data = {};
    let dataset = {
      data: sumsOflistingStats,
      backgroundColor: Item.statusColors().map(statusColor => statusColor.color)
    };

    data.labels = this.listingStats.map(item => item.status.capitalize());
    data.datasets = [];
    data.datasets.push(dataset)

    return data;
  }
}


class ListingStat {
  constructor(status, sum, percentage) {
    this.status = status;
    this.sum = sum;
    this.percentage = percentage;
  }

  get color() {
    return Item.statusColors().find(statusColor => statusColor.status == this.status).color
  }
}

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}